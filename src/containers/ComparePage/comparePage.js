import React from "react";
import "./comparePage.scss";

import { connect } from "react-redux";
import { getAllProducts, removeProduct } from "../../actions/products";

// Components
import ProductSelection from "../../components/ProductSelection";
import ProductHeader from "../../components/ProductHeader";
import ProductBadges from "../../components/ProductBadges";
import ProductSpecsRow from "../ProductSpecsRow";

// Product specs that should get compared
const comparisonSpecs = [
  "Toepassing",
  "Hardheid",
  "Artikelnummer",
  "stepQuantity",
  "Kleur",
  "Temperatuurgebied",
  "Materiaal",
  "Snoerdikte",
  "Inwendige diameter",
  "Maat volgens AS568"
];

const ComparePageTitle = ({ isLoading, numberOfProducts }) => {
  return (
    <h1>
      {!isLoading ? (
        <React.Fragment>
          {numberOfProducts === 1 ? "1 product " : numberOfProducts + " producten "}
        </React.Fragment>
      ) : (
        <React.Fragment>
          <span>Producten</span>
        </React.Fragment>
      )}
      vergelijken
    </h1>
  );
};

class ComparePage extends React.Component {
  componentDidMount() {
    if (this.props.productList.length === 0) {
      this.props.getAllProducts();
    }
  }

  render() {
    const { isLoadingProducts, productList, selectedProducts } = this.props;
    return (
      <div className="ComparePage">
        <ComparePageTitle isLoading={isLoadingProducts} numberOfProducts={productList.length} />
        <div className="ComparePage__comparison-table">
          <table>
            <thead>
              <tr>
                <th>
                  <ProductSelection />
                </th>
                {!isLoadingProducts ? (
                  <React.Fragment>
                    {selectedProducts.map((product, i) => (
                      <th key={i} className="ComparePage__header-wrapper">
                        <button onClick={() => this.props.removeProduct(product)}>Remove</button>
                        <ProductHeader
                          image={product.productImage}
                          name={product.name}
                          price={product.listPrice}
                          priceLabel={`per ${product.uom} / excl. btw`}
                        />
                      </th>
                    ))}
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <th>Loading product info</th>
                  </React.Fragment>
                )}
              </tr>
              <tr>
                <th>Keurmerk</th>
                {!isLoadingProducts ? (
                  <React.Fragment>
                    {selectedProducts.map((product, i) => (
                      <th key={i}>
                        <ProductBadges badges={product.badges} />
                      </th>
                    ))}
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <th>Loading badges</th>
                  </React.Fragment>
                )}
              </tr>
            </thead>
            <tbody>
              {// sort product spec names in alphabetical order, ignoring the case
              comparisonSpecs
                .sort(function(a, b) {
                  return a.toLowerCase().localeCompare(b.toLowerCase());
                })
                .map((feature, i) => (
                  <React.Fragment key={i}>
                    {!isLoadingProducts ? (
                      <React.Fragment>
                        <ProductSpecsRow
                          specs={selectedProducts.map((product, i) => product[feature])}
                          specName={feature}
                        />
                      </React.Fragment>
                    ) : (
                      <React.Fragment>
                        <tr>
                          <td>Loading</td>
                        </tr>
                      </React.Fragment>
                    )}
                  </React.Fragment>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoadingProducts: state.comparePage.isLoadingProducts,
  productList: state.comparePage.productList,
  selectedProducts: state.comparePage.selectedProducts
});

export default connect(mapStateToProps, { getAllProducts, removeProduct })(ComparePage);
