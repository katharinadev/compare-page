import React from "react";
import "./App.scss";

// State management
import { Provider } from "react-redux";
import store from "../../store";

// Routing
import { HashRouter as Router, Route, Switch, Link } from "react-router-dom";

import ComparePage from "../ComparePage";

// Component for 404 page
const PageNotFound = () => {
  return (
    <div className="App__page-not-found">
      <h1>404: Page Not Found</h1>
      <Link to="/">Home</Link>
    </div>
  );
};

class App extends React.Component {
  render() {
    return (
      <main className="App">
        <Provider store={store}>
          <Router basename="/">
            <Switch>
              <Route path="/" exact component={ComparePage} />
              <Route component={PageNotFound} />
            </Switch>
          </Router>
        </Provider>
      </main>
    );
  }
}

export default App;
