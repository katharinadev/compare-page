import React from "react";
import "./ProductSpecsRow.scss";

class ProductSpecsRow extends React.Component {
  render() {
    const { specs, specName } = this.props;

    let sameValues = false;
    if (specs.length !== 0) {
      sameValues = !this.props.specs.reduce(function(a, b) {
        return a === b ? a : NaN;
      });
    }

    return (
      <tr className={sameValues === false ? "ProductSpecsRow" : "ProductSpecsRow--highlighted"}>
        <td>{specName}</td>
        <React.Fragment>
          {specs.map((spec, i) => (
            <td key={i}>{spec}</td>
          ))}
        </React.Fragment>
      </tr>
    );
  }
}

export default ProductSpecsRow;
