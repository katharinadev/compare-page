import { combineReducers } from "redux";

import comparePage from "./reducers/comparePage";

export default combineReducers({
  comparePage
});
