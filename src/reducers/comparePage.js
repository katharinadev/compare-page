import { GET_ALL_PRODUCTS, REMOVE_PRODUCT } from "../constants.js";

const initialState = {
  productList: [],
  isLoadingProducts: true,
  selectedProducts: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_PRODUCTS:
      return {
        ...state,
        productList: action.payload,
        isLoadingProducts: false,
        selectedProducts: action.payload
      };
    case REMOVE_PRODUCT:
      return {
        ...state,
        selectedProducts: state.selectedProducts.filter(
          (product, index) => product !== action.product
        )
      };
    default:
      return state;
  }
}
