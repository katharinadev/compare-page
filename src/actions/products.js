import axios from "axios";
import { GET_ALL_PRODUCTS, REMOVE_PRODUCT } from "../constants";

export const getAllProducts = () => dispatch => {
  axios
    .get("http://5c35e7f96fc11c0014d32fcd.mockapi.io/compare/products")
    .then(res => {
      dispatch({
        type: GET_ALL_PRODUCTS,
        payload: res.data.products
      });
    })
    .catch(err => console.log(err));
};

export const removeProduct = product => ({
  type: REMOVE_PRODUCT,
  product
});
