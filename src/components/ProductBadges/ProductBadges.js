import React from "react";
import "./ProductBadges.scss";

const ProductBadges = ({ badges }) => {
  // Create Array of all bafges URLs from string
  const badgesArray = badges.split("|");
  return (
    <div className="ProductBadges">
      {badgesArray.map((badge, i) => (
        <img className="ProductBadges__image" src={badge} alt="Badge" key={i} />
      ))}
    </div>
  );
};

export default ProductBadges;
