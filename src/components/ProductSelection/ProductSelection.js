import React from "react";
import "./ProductSelection.scss";

import { connect } from "react-redux";
import { getAllProducts } from "../../actions/products";

const ProductSelectionCheckbox = ({ id, label, isChecked }) => {
  return (
    <div className="ProductSelectionCheckbox">
      <input
        type="checkbox"
        id={`product${id}`}
        name="selection"
        value={`product${id}`}
        checked={isChecked}
        //onChange={updateSelection}
      />
      <label htmlFor={`product${id}`}>{label}</label>
    </div>
  );
};

class ProductSelection extends React.Component {
  render() {
    const { isLoadingProducts, productList, selectedProducts } = this.props;

    return (
      <fieldset className="ProductSelection">
        <legend>Je selectie</legend>

        {!isLoadingProducts ? (
          <React.Fragment>
            {productList.map((product, i) => (
              <ProductSelectionCheckbox
                id={i}
                label={product.name}
                //check if product is in array
                isChecked={selectedProducts.indexOf(product) !== -1}
                key={i}
              />
            ))}
          </React.Fragment>
        ) : (
          <React.Fragment>
            <p>Loading selected products</p>
          </React.Fragment>
        )}
      </fieldset>
    );
  }
}

const mapStateToProps = state => ({
  isLoadingProducts: state.comparePage.isLoadingProducts,
  productList: state.comparePage.productList,
  selectedProducts: state.comparePage.selectedProducts
});

export default connect(mapStateToProps, { getAllProducts })(ProductSelection);
