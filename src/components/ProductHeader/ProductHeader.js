import React from "react";
import "./ProductHeader.scss";

const ProductHeader = ({ image, name, price, priceLabel }) => {
  return (
    <div className="ProductHeader">
      <img className="ProductHeader__image" src={image} alt={name} />
      <div className="ProductHeader__name">{name}</div>
      <div className="ProductHeader__price">{price}</div>
      <div className="ProductHeader__price-label">{priceLabel}</div>
    </div>
  );
};

export default ProductHeader;
